# Website v6
The source code for my portfolio website on [tobiaszehntner.art](http://www.tobiaszehntner.art).

Based on [HTML5 Boilerplate](https://html5boilerplate.com) and [Bootstrap](http://getbootstrap.com).
Built with [Jekyll](http://jekyllrb.com) and hosted on [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/).

## Thanks
- [Ekko Lightbox](https://github.com/ashleydw/lightbox)
- [Atom](https://atom.io)
- Ben Frain's [scroll tutorial](https://benfrain.com/a-horizontal-scrolling-navigation-pattern-for-touch-and-mouse-with-moving-current-indicator/)
- [Dragscroll](https://github.com/asvd/dragscroll/blob/master/dragscroll.js)

## Contact
Please contact me at [studio@tobiaszehntner.art](mailto:studio@tobiaszehntner.art) for questions, suggestions or reporting bugs.

## Copyright
- Images, videos and artworks ©2007-2020 Tobias Zehntner
- Source code published under [License AGPL-3.0](https://www.gnu.org/licenses/agpl.html) or later

## How To

### Jekyll

#### Install

```bash
$ brew upgrade
$ gem install bundler --pre
$ rvm install ruby
$ bundle update
$ gem install jekyll bundler
```

#### Update

```bash
$ bundle update
```

#### Serve

```bash
$ bundle exec jekyll serve 
```
#### Errors

- `Error: /usr/local must be writable!`
  - `sudo chown -R $(whoami) /usr/local`

### Atom

#### Packages

```apm install highlight-selected indent-guide-improved less-than-slash pigments rainbow-delimiters linter minimap autosave-onchange git-blame language-liquid jekyll-syntax-highlighting atom-beautify atom-minify uglify-html```
