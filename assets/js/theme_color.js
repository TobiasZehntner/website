---
---
{% unless site.color_mode == 'fixed' %}
{% if site.color_mode == 'random' %}
// Get random hue
function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}
var hue = getRandomInt(0, 360)
{% endif %}

{% if site.color_mode == 'date' %}
// get hue of day in year
var now = new Date();
var start = new Date(now.getFullYear(), 0, 0);
var diff = now - start;
var oneDay = 1000 * 60 * 60 * 24;
var day = Math.floor(diff / oneDay);
// Map day of year (1-365) to hue value (0-360)
var hue = Math.round(day / 365 * 360);
// Clamp number for leap years (366 days)
if (hue > 360) {
    hue = 360
}
{% endif %}
var secondary_diff = {{ site.bg_color_gradient_hue_diff }}
secondary_hue = hue + secondary_diff
border_hue = hue + (secondary_diff / 2)
var hsl_border_string = 'hsl(' + border_hue + ', 100%, 50%)'
var hsl_primary_string = 'hsl(' + hue + ', 100%, 50%)'
var hsl_secondary_string = 'hsl(' + secondary_hue + ', 100%, 50%)'
var gradient_string = 'linear-gradient(120deg, ' + hsl_primary_string +' 0%, ' + hsl_secondary_string + ' 100%)'

// Convert HSL to RGB
var r, g, b, R, G, B, C, L;
var h = 1 / 360 * (hue + secondary_hue) / 2
var s = 1
var l = 0.5

var hue2rgb = function hue2rgb(p, q, t){
    if(t < 0) t += 1;
    if(t > 1) t -= 1;
    if(t < 1/6) return p + (q - p) * 6 * t;
    if(t < 1/2) return q;
    if(t < 2/3) return p + (q - p) * (2/3 - t) * 6;
    return p;
}

var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
var p = 2 * l - q;
r = hue2rgb(p, q, h + 1.0/3.0);
g = hue2rgb(p, q, h);
b = hue2rgb(p, q, h - 1.0/3.0);
R = Math.round(r * 255);
G = Math.round(g * 255);
B = Math.round(b * 255);

// Set primary text color to black or white in contrast to background
C = [ R/255, G/255, B/255 ];
for ( var i = 0; i < C.length; ++i ) {
    if ( C[i] <= 0.03928 ) {
        C[i] = C[i] / 12.92
    } else {
        C[i] = Math.pow( ( C[i] + 0.055 ) / 1.055, 2.4);
    }
}
L = 0.2126 * C[0] + 0.7152 * C[1] + 0.0722 * C[2];

// L > 0.179 would be standard, 0.4 uses white text more often
var text_color
if ( L > 0.4 ) {
    text_color = '#333';
} else {
    text_color = '#FFF';
    $('#email_input').addClass('text-light')
}

// Set variables
var html = document.getElementsByTagName('html')[0];
html.style.setProperty('--text-on-color', text_color)
html.style.setProperty('--border-color', hsl_border_string)
html.style.setProperty('--bg-color-gradient', gradient_string)

// Set classes
$('.color-fill').css({
  'color': text_color,
  'background-image': gradient_string,
})
$('.color-border').css({
  'border-color': hsl_border_string,
})
$('.color-form').css({
  'border-color': text_color,
  'color': text_color,
})
{% endunless %}
