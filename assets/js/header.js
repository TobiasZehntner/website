---
---
// Collapse mobile navbar when touching outside (Chrome gives warning about using touchstart)
// $(document).on('click touchstart', function (e) {
//     if ($(".navbar-collapse").hasClass("show") && !$(e.target).is('nav')) {
//     	$('.navbar-collapse').collapse('hide');
//     }
// });

// Show menu on scroll up, hide on scroll down
var didScroll;
var lastScrollTop = 0;
var delta = 5;
var navbarHeight = $('header').outerHeight();

$(window).scroll(function(event){
    didScroll = true;
});

setInterval(function() {
    if (didScroll) {
        hasScrolled();
        didScroll = false;
    }
}, 250);

function hasScrolled() {
    var st = $(this).scrollTop();

    // Make sure they scroll more than delta
    if(Math.abs(lastScrollTop - st) <= delta)
        return;

    // If they scrolled down and are past the navbar, add class .nav-up.
    // This is necessary so you never see what is "behind" the navbar.
    if (st > lastScrollTop && st > navbarHeight){
        // Scroll Down
        $('header').removeClass('nav-down').addClass('nav-up');
        $('#navbarSupportedContent').removeClass('show')
    } else {
        // Scroll Up
        if(st + $(window).height() < $(document).height()) {
            $('header').removeClass('nav-up').addClass('nav-down');
        }
    }
    lastScrollTop = st;
}
// Show nav on hovering at top of page
$(document).ready(function(){
    $('#header_space').hover(function(){
        $('header').removeClass('nav-up').addClass('nav-down');
        }, function(){
        $('header').removeClass('nav-down').addClass('nav-up');
    });
});

// Highlight menu item if clicked or scrolled to
$('header nav a').on('click', function(event) {
    $('header nav a').removeClass('active');
    $(this).addClass('active');
});

$(window).on('scroll', function() {
    $('section').each(function() {
        if($(window).scrollTop() >= $(this).offset().top) {
            var id = $(this).attr('id');
            $('header nav a').removeClass('active');
            $('header nav a[href="{{ site.baseurl }}/#'+ id +'"]').addClass('active');
        }
    });
});