---
---
// Follow-me hover
$(document).ready(function() {
    $('#follow_instagram').hover(
    function() {
        $('#follow_me').text('my pictures');
    }, function() {
        $('#follow_me').text('me');
    });
    $('#follow_gitlab').hover(
    function() {
        $('#follow_me').text('my code');
    }, function() {
        $('#follow_me').text('me');
    });
    $('#follow_linkedin').hover(
    function() {
        $('#follow_me').text('my career');
    }, function() {
        $('#follow_me').text('me');
    });
});

// Enable popover (needs popper.js)
$(function () {
  $('[data-toggle="popover"]').popover()
})
$('.popover-dismiss').popover({
  trigger: 'focus'
})
