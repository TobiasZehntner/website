---
---
// Scroll buttons
function determineOverflow(content, container) {
    var containerMetrics = container.getBoundingClientRect();
    var containerMetricsRight = Math.floor(containerMetrics.right);
    var containerMetricsLeft = Math.floor(containerMetrics.left);
    var contentMetrics = content.getBoundingClientRect();
    var contentMetricsRight = Math.floor(contentMetrics.right);
    var contentMetricsLeft = Math.floor(contentMetrics.left);
    if (containerMetricsLeft > contentMetricsLeft && containerMetricsRight < contentMetricsRight) {
        return "both";
    } else if (contentMetricsLeft < containerMetricsLeft) {
        return "left";
    } else if (contentMetricsRight > containerMetricsRight) {
        return "right";
    } else {
        return "none";
    }
}

var project_scroll_bar = document.getElementById("project_scroll_bar");
var project_scroll_content = document.getElementById("project_scroll_content");

project_scroll_bar.setAttribute("data-overflowing", determineOverflow(project_scroll_content, project_scroll_bar));

// Handle the scroll of the horizontal container
var project_last_known_scroll_position = 0;
var project_ticking = false;

function scroll_project(scroll_pos) {
    project_scroll_bar.setAttribute("data-overflowing", determineOverflow(project_scroll_content, project_scroll_bar));
}
project_scroll_bar.addEventListener("scroll", function() {
    project_last_known_scroll_position = window.scrollY;
    if (!project_ticking) {
        window.requestAnimationFrame(function() {
            scroll_project(project_last_known_scroll_position);
            project_ticking = false;
        });
    }
    project_ticking = true;
});

// Get equivalent to CSS vw (screen width)
function vw(v) {
  var w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
  return (v * w) / 100;
};

var SETTINGS = {
    navBarTravelling: false,
    navBarDirection: "",
    navBarTravelDistance: vw(70),
};



// Our advancer buttons
var project_btn_scroll_left = document.getElementById("project_btn_scroll_left");
var project_btn_scroll_right = document.getElementById("project_btn_scroll_right");

project_btn_scroll_left.addEventListener("click", function() {
    // If in the middle of a move return
    if (SETTINGS.navBarTravelling === true) {
        return;
    }
    // If we have content overflowing both sides or on the left
    if (determineOverflow(project_scroll_content, project_scroll_bar) === "left" || determineOverflow(project_scroll_content, project_scroll_bar) === "both") {
        // Find how far this panel has been scrolled
        var availableScrollLeft = project_scroll_bar.scrollLeft;
        // If the space available is less than two lots of our desired distance, just move the whole amount
        // otherwise, move by the amount in the settings
        if (availableScrollLeft < SETTINGS.navBarTravelDistance * 2) {
            project_scroll_content.style.transform = "translateX(" + availableScrollLeft + "px)";
        } else {
            project_scroll_content.style.transform = "translateX(" + SETTINGS.navBarTravelDistance + "px)";
        }
        // We do want a transition (this is set in CSS) when moving so remove the class that would prevent that
        project_scroll_content.classList.remove("scroll-content-no-transition");
        // Update our settings
        SETTINGS.navBarTravelDirection = "left";
        SETTINGS.navBarTravelling = true;
    }
    // Now update the attribute in the DOM
    project_scroll_bar.setAttribute("data-overflowing", determineOverflow(project_scroll_content, project_scroll_bar));
});
project_btn_scroll_right.addEventListener("click", function() {
    // If in the middle of a move return
    if (SETTINGS.navBarTravelling === true) {
        return;
    }
    // If we have content overflowing both sides or on the right
    if (determineOverflow(project_scroll_content, project_scroll_bar) === "right" || determineOverflow(project_scroll_content, project_scroll_bar) === "both") {
        // Get the right edge of the container and content
        var navBarRightEdge = project_scroll_content.getBoundingClientRect().right;
        var navBarScrollerRightEdge = project_scroll_bar.getBoundingClientRect().right;
        // Now we know how much space we have available to scroll
        var availableScrollRight = Math.floor(navBarRightEdge - navBarScrollerRightEdge);
        // If the space available is less than two lots of our desired distance, just move the whole amount
        // otherwise, move by the amount in the settings
        if (availableScrollRight < SETTINGS.navBarTravelDistance * 2) {
            project_scroll_content.style.transform = "translateX(-" + availableScrollRight + "px)";
        } else {
            project_scroll_content.style.transform = "translateX(-" + SETTINGS.navBarTravelDistance + "px)";
        }
        // We do want a transition (this is set in CSS) when moving so remove the class that would prevent that
        project_scroll_content.classList.remove("scroll-content-no-transition");
        // Update our settings
        SETTINGS.navBarTravelDirection = "right";
        SETTINGS.navBarTravelling = true;
    }
    // Now update the attribute in the DOM
    project_scroll_bar.setAttribute("data-overflowing", determineOverflow(project_scroll_content, project_scroll_bar));
});
project_scroll_content.addEventListener(
    "transitionend",
    function() {
        // get the value of the transform, apply that to the current scroll position (so get the scroll pos first) and then remove the transform
        var styleOfTransform = window.getComputedStyle(project_scroll_content, null);
        var tr = styleOfTransform.getPropertyValue("-webkit-transform") || styleOfTransform.getPropertyValue("transform");
        // If there is no transition we want to default to 0 and not null
        var amount = Math.abs(parseInt(tr.split(",")[4]) || 0);
        project_scroll_content.style.transform = "none";
        project_scroll_content.classList.add("scroll-content-no-transition");
        // Now lets set the scroll position
        if (SETTINGS.navBarTravelDirection === "left") {
            project_scroll_bar.scrollLeft = project_scroll_bar.scrollLeft - amount;
        } else {
            project_scroll_bar.scrollLeft = project_scroll_bar.scrollLeft + amount;
        }
        SETTINGS.navBarTravelling = false;
    },
    false
);
project_scroll_content.addEventListener("click", function(e) {
    // Make an array from each of the links in the nav
    var links = [].slice.call(document.querySelectorAll(".project"));
    // Turn all of them off
    links.forEach(function(item) {
        item.setAttribute("aria-selected", "false");
    })
    // Set the clicked one on
    e.target.setAttribute("aria-selected", "true");
});

// Dragscroll https://github.com/asvd/dragscroll/blob/master/dragscroll.js

(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        define(['exports'], factory);
    } else if (typeof exports !== 'undefined') {
        factory(exports);
    } else {
        factory((root.dragscroll = {}));
    }
}(this, function (exports) {
    var _window = window;
    var _document = document;
    var mousemove = 'mousemove';
    var mouseup = 'mouseup';
    var mousedown = 'mousedown';
    var EventListener = 'EventListener';
    var addEventListener = 'add'+EventListener;
    var removeEventListener = 'remove'+EventListener;
    var newScrollX, newScrollY;

    var dragged = [];
    var reset = function(i, el) {
        for (i = 0; i < dragged.length;) {
            el = dragged[i++];
            el = el.container || el;
            el[removeEventListener](mousedown, el.md, 0);
            _window[removeEventListener](mouseup, el.mu, 0);
            _window[removeEventListener](mousemove, el.mm, 0);
        }

        // cloning into array since HTMLCollection is updated dynamically
        dragged = [].slice.call(_document.getElementsByClassName('dragscroll'));
        for (i = 0; i < dragged.length;) {
            (function(el, lastClientX, lastClientY, pushed, scroller, cont){
                (cont = el.container || el)[addEventListener](
                    mousedown,
                    cont.md = function(e) {
                        if (!el.hasAttribute('nochilddrag') ||
                            _document.elementFromPoint(
                                e.pageX, e.pageY
                            ) == cont
                        ) {
                            pushed = 1;
                            lastClientX = e.clientX;
                            lastClientY = e.clientY;

                            e.preventDefault();
                        }
                    }, 0
                );

                _window[addEventListener](
                    mouseup, cont.mu = function() {pushed = 0;}, 0
                );

                _window[addEventListener](
                    mousemove,
                    cont.mm = function(e) {
                        if (pushed) {
                            (scroller = el.scroller||el).scrollLeft -=
                                newScrollX = (- lastClientX + (lastClientX=e.clientX));
                            scroller.scrollTop -=
                                newScrollY = (- lastClientY + (lastClientY=e.clientY));
                            if (el == _document.body) {
                                (scroller = _document.documentElement).scrollLeft -= newScrollX;
                                scroller.scrollTop -= newScrollY;
                            }
                        }
                    }, 0
                );

                // Avoid click on dragscroll fix
                _window[addEventListener](
                    mouseup, cont.mu = function() {
                        pushed = 0;
                        // HERE
                        setTimeout(function(){ el.classList.remove("dragging"); }, 100);
                    }, 0
                );

                _window[addEventListener](
                    mousemove,
                    cont.mm = function(e) {
                        if (pushed) {
                            // HERE
                            el.classList.add("dragging");
                            (scroller = el.scroller||el).scrollLeft -=
                                newScrollX = (- lastClientX + (lastClientX=e.clientX));
                            scroller.scrollTop -=
                                newScrollY = (- lastClientY + (lastClientY=e.clientY));
                            if (el == _document.body) {
                                (scroller = _document.documentElement).scrollLeft -= newScrollX;
                                scroller.scrollTop -= newScrollY;
                            }
                        }
                    }, 0
                );
             })(dragged[i++]);
        }
    }


    if (_document.readyState == 'complete') {
        reset();
    } else {
        _window[addEventListener]('load', reset, 0);
    }

    exports.reset = reset;
}));


$('.dragscroll .project-link').click(function(event) {
    if ($(this).parents('.dragscroll').hasClass('dragging')) {
      event.preventDefault();
      return false;
    }
});
