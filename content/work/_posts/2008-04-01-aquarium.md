---
layout:       default
type:         project
permalink:    /work/aquarium
published:    true

title:        Aquarium
year:         2008
media:        Video
size:         "16:9"
time:         1.58 minutes

description:  "The video is a study of human behaviour in artificial, but existing places. The camera is on the sideline watching and documenting people in this seemingly unknown, but familiar space."

name:         aquarium
tags:
    - Video
repository:   
img-thumb:    aquarium-thumb.jpg
img-title:    aquarium1.jpg

inst-views:   false
img-hor1:
img-hor2:
img-ver1:
img-ver2:
img-ver3:

video-js:     true
video-poster: aquarium-poster.png
video-mp4:    aquarium.mp4
video-webm:   aquarium.webm
video-ogv:    aquarium.ogv


vimeo:
    - { id: 101715346, w: 1024, h: 576}
---

{% include project.html %}
