---
layout:       default
type:         project
permalink:    /work/zeitfenster
published:    true

title:        Zeitfenster
year:         2010
media:        Video loop, silent
size:         "16:9"
time:         9 minute loop

description:  "With the daily commute as a background, the  video tells of people on an apparently endless train, captured in their daily routine. While the protagonists of this repetitive movement do not seem to notice or care, their bodies become mere shadows of themselves."
tags:
    - Video
name:         zeitfenster
repository:   
img-thumb:    zeitfenster-thumb.jpg
img-title:    zeitfenster1.jpg

inst-views:   false
img-hor1:
img-hor2:
img-ver1:
img-ver2:
img-ver3:

video-js:     true
video-poster: zeitfenster-poster.png
video-mp4:    zeitfenster.mp4
video-webm:   zeitfenster.webm
video-ogv:    zeitfenster.ogv


vimeo:
    - { id: 102230972, w: 1024, h: 576}
---

{% include project.html %}
