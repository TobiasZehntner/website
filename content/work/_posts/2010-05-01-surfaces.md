---
layout:       default
type:         project
permalink:    /work/surfaces
published:    true

title:        Surfaces
year:         2010
media:        Glass, water, oil
size:         20 x 40 x 20 cm
time:

description:  
tags:
    - Sculpture
name:         surfaces
repository:   
img-thumb:    surfaces-thumb.jpg
img-title:    surfaces1.jpg

inst-views:   true
img-hor1:     surfaces2.jpg
img-hor2:     surfaces3.jpg
img-ver1:     surfaces4.jpg
img-ver2:
img-ver3:

video-js:     false
video-poster:
video-mp4:
video-webm:
video-ogv:    

vimeo:
    - { id: 324246422, w: 600, h: 338}
---

{% include project.html %}
