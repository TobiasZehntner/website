---
layout:       default
type:         project
permalink:    /work/wave-water
published:    true

title:        Wave (water)
year:         2011
media:        Glass, metal, water, oil, pump
size:         130 x 60 x 45 cm
time:

description:  "A tank filled half with water, half with light mineral oil. The surface between the two liquids is transparent when looked at from above and below, but fully reflective when seen through the side glass of the tank. The surface is animated by an underwater propeller which has a metal plate on top for equal distribution of the current. The viscosity of the oil makes the generated waves appear in slow motion."
tags:
    - Kinetic
    - Sculpture
name:         wavewater
repository:   
img-thumb:    wavewater-thumb.jpg
img-title:    wavewater1.jpg

inst-views:   true
img-hor1:
img-hor2:
img-ver1:     wavewater2.jpg
img-ver2:     wavewater3.jpg
img-ver3:

video-js:     true
video-poster: wavewater-poster.png
video-mp4:    wavewater.mp4
video-webm:   wavewater.webm
video-ogv:    wavewater.ogv


vimeo:
    - { id: 102230974, w: 720, h: 576}
---

{% include project.html %}
