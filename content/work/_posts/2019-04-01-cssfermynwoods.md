---
layout:       default
type:         project
permalink:    /work/css-fermynwoods
published:    false

title:        Swing
year:         2018
media:        Cascading Stylesheet File
size:         
time:

description:  "A CSS stylesheet Commissioned by Fermynwoods Contemporary Art to be applied to the organizations [website](http://www.fermynwoods.org)."

tags:
    - Virtual
name:         cssfermynwoods
repository:   2019-fermynwoods

vimeo:     false     
---

{% include project.html %}
