---
layout:       default
type:         project
permalink:    /work/swingaarau
published:    true

title:        Swing
year:         2018
media:        Halogen bulb, cable, custom electronics
size:         "Cable: 12m"
time:

description:  "As a new iteration of the work [*Swing* (2014)]({{ site.baseurl }}/work/swing), the swinging light bulb was set up for the collaborative installation and performance *Multiplication of Rhythm* at the Alte Reithalle in Aarau, Switzerland. Its oscillating rhythm served as a starting point for participating artists, musicians and dancers to develop a performative exhibition."

tags:
    - Kinetic
    - Light
    - Installation
name:         swingaarau
repository:   2018-swing
img-thumb:    swing-thumb.jpg
img-title:    swing1.jpg

inst-views:   false
img-hor1:
img-hor2:
img-ver1:
img-ver2:
img-ver3:

video-js:     true
video-poster: swing-poster.png
video-mp4:    swing.mp4
video-webm:   swing.webm
video-ogv:    swing.ogv


vimeo:
    - { id: 309966981, w: 1920, h: 1080}
---

{% include project.html %}
