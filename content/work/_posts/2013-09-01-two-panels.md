---
layout:       default
type:         project
permalink:    /work/two-panels
published:    false

title:        Two Panels
year:         2013
media:        LEDs, microcontroller, wood, fabric, reflective fabric
size:         45 x 48 inch each (2)
time:         2.30 minute loop

description:  "Two panels facing each other. One contains 300 LEDs that each blink with a slightly different frequency. The LEDs sync about every 2.5 minutes. While they all fall out of sync and back again, they form unique patterns that change with each cycle. The other panel consists of reflective fabric. It throws the emitted light from the first panel back. But only if the viewer stands exactly between the panels, otherwise it keeps its neutral grey color. It gathers the emitted light and shows it on its panel, changing from on/off to fading and back."
tags:
    - Light
    - Sculpture
name:         twopanels
repository:   
img-thumb:    twopanels-thumb.jpg
img-title:    twopanels1.jpg

inst-views:   true
img-hor1:
img-hor2:
img-ver1:     twopanels2.jpg
img-ver2:     twopanels3.jpg
img-ver3:

video-js:     true
video-poster: twopanels-poster.png
video-mp4:    twopanels.mp4
video-webm:   twopanels.webm
video-ogv:    twopanels.ogv


vimeo:
    - { id: 102416626, w: 1280, h: 720}  
---

{% include project.html %}
