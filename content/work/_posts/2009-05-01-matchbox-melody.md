---
layout:       default
type:         project
permalink:    /work/matchbox-melody
published:    true

title:        Matchbox Melody
year:         2009
media:        4-channel video loop, silent
size:         Dimensions variable
time:         15.08 minute loop

description:  "With an architectural feature of London’s city as starting point, this video samples the artificial space into a new façade of choreographed musical movement. While it brings up links to computer and games, it keeps the bond to the images origin, what gives the work an odd notion of scale."
tags:
    - Video
    - Installation
name:         matchbox
repository:   
img-thumb:    matchbox-thumb.jpg
img-title:    matchbox1.jpg

inst-views:   true
img-hor1:     matchbox2.jpg
img-hor2:     matchbox3.jpg
img-ver1:
img-ver2:
img-ver3:

video-js:     true
video-poster: matchbox-poster.png
video-mp4:    matchbox.mp4
video-webm:   matchbox.webm
video-ogv:    matchbox.ogv


vimeo:
    - { id: 102230971, w: 1280, h: 720}
---

{% include project.html %}
