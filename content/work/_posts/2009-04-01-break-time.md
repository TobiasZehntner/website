---
layout:       default
type:         project
permalink:    /work/break-time
published:    true

title:        Break Time
year:         2009
media:        3-channel video loop, silent
size:         "16:9"
time:         16.50 minute loop

description:  "A 3-channel installation featuring office workers on their smoking break. The sampled and choreographed sequences follow the slow and arbitrary movements of the distant and lonely smokers and focus on the act of passing time."
tags:
    - Video
    - Multi-Screen
name:         breaktime
repository:   
img-thumb:    breaktime-thumb.jpg
img-title:    breaktime1.jpg

inst-views:   false
img-hor1:
img-hor2:
img-ver1:
img-ver2:
img-ver3:

video-js:     true
video-poster: breaktime-poster.png
video-mp4:    breaktime.mp4
video-webm:   breaktime.webm
video-ogv:    breaktime.ogv


vimeo:
    - { id: 101716074, w: 840, h: 160}
---

{% include project.html %}
