---
layout:       default
type:         project
permalink:    /work/wave-light
published:    true

title:        Wave (light)
year:         2011
media:        Room installation; Projector, iridescent acrylic, metal
size:         280 x 400 x 580 cm
time:         20 minute loop

description:  "A room built like a narrowing box, tilted so the viewer stands on a flat ground and making her/him the only ‘object’ in the room in an upright position. A sheet of wave-form moulded iridescent acylic is hanged in the rear half of the room. A projector on a metal stand projects a vertical and a horizontal line (which change slowly in color) onto the sheet, ‘scanning’ it by moving horizontally and vertically respectively. The residual light of the projector illuminates the rear quarter of the room, which makes it appear in a haze. The projected lines are reflected from the moulded acrylic onto the side and back walls of the room, creating moving ‘light waves’."
tags:
    - Video
    - Light
    - Installation
name:         wavelight
repository:   
img-thumb:    wavelight-thumb.jpg
img-title:    wavelight1.jpg

inst-views:   true
img-hor1:     wavelight2.jpg
img-hor2:     wavelight3.jpg
img-ver1:
img-ver2:
img-ver3:

video-js:     true
video-poster: wavelight-poster.png
video-mp4:    wavelight.mp4
video-webm:   wavelight.webm
video-ogv:    wavelight.ogv


vimeo:
    - { id: 102416624, w: 768, h: 576}
---

{% include project.html %}
