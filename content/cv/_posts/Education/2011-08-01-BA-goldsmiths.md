---
type: cv
category: education
year: 2011
---

Bachelor of Arts: Art Practice, First Class Honours, [Goldsmiths](https://www.gold.ac.uk/), University of London, UK
