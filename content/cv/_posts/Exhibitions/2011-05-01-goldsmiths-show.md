---
type: cv
category: exhibitions
year: 2011
project:
    - wavewater
    - wavelight
---

*Undergraduate Degree Show*, Goldsmiths, London, UK
